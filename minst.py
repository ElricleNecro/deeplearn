#!/usr/bin/env python
# -*- coding: utf-8 -*-


"""Implementation of a neural network able to recognise number from the MINST
data set.
"""

import numpy as np

from dlearn.train import train
from dlearn.nn import NeuralNet
from dlearn.layers import SoftMax, Linear
from dlearn.optim import SGD


def read_label(f_name):
    from os.path import getsize
    f_size = getsize(f_name)

    with open(f_name, "rb") as fich:
        magic = int.from_bytes(fich.read(4), byteorder='big')
        if magic != 2049:
            raise ValueError(
                f"MagicNumber is not what was expected: {magic} instead of 2049."
            )

        number = int.from_bytes(fich.read(4), byteorder='big')

        data = np.fromfile(fich, dtype=np.uint8, count=f_size - 16)

    return number, data


def read_data(f_name):
    from os.path import getsize
    f_size = getsize(f_name)

    with open(f_name, "rb") as fich:
        magic = int.from_bytes(fich.read(4), byteorder='big')
        if magic != 2051:
            raise ValueError(
                f"MagicNumber is not what was expected: {magic} instead of 2051."
            )

        number = int.from_bytes(fich.read(4), byteorder='big')
        rows = int.from_bytes(fich.read(4), byteorder='big')
        cols = int.from_bytes(fich.read(4), byteorder='big')

        data = np.fromfile(fich, dtype=np.uint8, count=f_size - 16)

    return number, rows, cols, data


size, rows, cols, inputs = read_data(
    "~/Téléchargements/train-images-idx3-ubyte"
)
t_size, targets = read_label(
    "~/Téléchargements/train-labels-idx1-ubyte"
)

inputs.shape = (size, rows*cols)

net = NeuralNet([
    Linear(input_size=10, output_size=5),
    Linear(input_size=5, output_size=2),
    SoftMax(),
])

train(
    net,
    inputs,
    targets,
    num_epochs=5000
)
