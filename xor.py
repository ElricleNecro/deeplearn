#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""The xor problem.

An example of a function that cannot be learned with a simple linear model.
"""


import numpy as np

from dlearn.train import train
from dlearn.nn import NeuralNet
from dlearn.layers import Tanh, Linear


inputs = np.array([
    [0, 0],
    [1, 0],
    [0, 1],
    [1, 1],
])

targets = np.array([
    [1, 0],
    [0, 1],
    [0, 1],
    [1, 0],
])

net = NeuralNet(
    [
        Linear(input_size=2, output_size=2),
        Tanh(),
        Linear(input_size=2, output_size=2)
    ]
)

train(net, inputs, targets)

for x, y in zip(inputs, targets):
    predicted = net.forward(x)
    print(x, predicted, y, sep=" => ")
