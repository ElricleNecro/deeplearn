# -*- coding: utf-8 -*-

"""We feed inputs into the network in batches.

This module contain tools for iterating over data in batches.
"""

import numpy as np

from abc import ABCMeta, abstractmethod
from typing import Iterator, NamedTuple
from .tensor import Tensor


Batch = NamedTuple("Batch", [("inputs", Tensor), ("targets", Tensor)])


class DataIterator(metaclass=ABCMeta):

    @abstractmethod
    def __call__(self, inputs: Tensor, targets: Tensor) -> Iterator[Batch]:
        raise NotImplementedError


class BatchIterator(DataIterator):
    def __init__(self, batch_size: int = 32, shuffle: bool = True) -> None:
        super(DataIterator, self).__init__()

        self._batch_size = batch_size
        self._shuffle = shuffle

    def __call__(self, inputs: Tensor, targets: Tensor) -> Iterator[Batch]:
        starts = np.arange(0, len(inputs), self._batch_size)

        if self._shuffle:
            np.random.shuffle(starts)

        for start in starts:
            end = start + self._batch_size

            batch_inputs = inputs[start:end]
            batch_targets = targets[start:end]

            yield Batch(batch_inputs, batch_targets)
