# -*- coding: utf-8 -*-

"""A tensor is a generalisation of matrix to multi dimensionnal system.
"""

from numpy import ndarray as Tensor


__all__ = ["Tensor"]
