# -*- coding: utf-8 -*-

"""A loss function measure how good our predictions are. We use this to adjust
the parameters of the network.
"""

import numpy as np

from abc import ABCMeta, abstractmethod

from .tensor import Tensor


class Loss(metaclass=ABCMeta):
    @abstractmethod
    def loss(self, predicted: Tensor, actual: Tensor) -> float:
        raise NotImplementedError

    @abstractmethod
    def grad(self, predicted: Tensor, actual: Tensor) -> Tensor:
        raise NotImplementedError


class TSE(Loss):
    """Total squared error.
    """

    def loss(self, predicted: Tensor, actual: Tensor) -> float:
        return np.sum((predicted - actual) ** 2)

    def grad(self, predicted: Tensor, actual: Tensor) -> Tensor:
        return 2 * (predicted - actual)
