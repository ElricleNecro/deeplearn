# -*- coding: utf-8 -*-

"""An optimiser is used to adjust the parameters of the network based on the
gradients computed during the back propagation.
"""

from abc import ABCMeta, abstractmethod
from .nn import NeuralNet


class Optimiser(metaclass=ABCMeta):
    @abstractmethod
    def step(self, net: NeuralNet) -> None:
        raise NotImplementedError


class SGD(Optimiser):
    """Stochastic gradient descent optimiser.
    """

    def __init__(self, lr: float = 0.01) -> None:
        super(SGD, self).__init__()
        self._lr = lr

    def step(self, net: NeuralNet) -> None:
        for param, grad in net.params_and_grads():
            param -= self._lr * grad
