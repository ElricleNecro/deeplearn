# -*- coding: utf-8 -*-

"""A neural network is made of layers.

Each layer need to pass its inputs forward and propagate gradients backward.
A simple neural network might look like:
    inputs -> Linear -> tanh -> Linear -> output
"""

import numpy as np

from abc import ABCMeta, abstractmethod
from typing import Dict, Callable, Tuple
from .tensor import Tensor


F = Callable[[Tensor], Tensor]

__all__ = [
    "Layer",
    "Activation",

    "Linear",
    "Tanh",
    "SoftMax",
    "Convolution",
]


class Layer(metaclass=ABCMeta):
    def __init__(self) -> None:
        self.params: Dict[str, Tensor] = dict()
        self.grads:  Dict[str, Tensor] = dict()

    @abstractmethod
    def forward(self, inputs: Tensor) -> Tensor:
        """Produce the outputs corresponding to the given inputs.
        """
        raise NotImplementedError

    @abstractmethod
    def backward(self, grad: Tensor) -> Tensor:
        """Back propagate the gradient through the current layer.
        """
        raise NotImplementedError


class Linear(Layer):
    """Compute outputs = inputs @ w + b
    """

    def __init__(self, input_size: int, output_size: int) -> None:
        super(Linear, self).__init__()

        # Inputs will be (batch_size, input_size)
        # outputs will be (batch_size, input_size)
        self.params["w"] = np.random.randn(input_size, output_size)
        self.params["b"] = np.random.randn(output_size)

    def forward(self, inputs: Tensor) -> Tensor:
        """ f(x) = x @ w + b
        """
        self.inputs = inputs
        return inputs @ self.params["w"] + self.params["b"]

    def backward(self, grad: Tensor) -> Tensor:
        """y = f(x) and x = a @ b + c =>
            dy/da = f'(x) @ b.T
            dy/db = a.T @ f'(x)
            dy/dc = f'(x) (kind of)
        """
        self.grads["b"] = np.sum(grad, axis=0)
        self.grads["w"] = self.inputs.T @ grad

        return grad @ self.params["w"].T


class Activation(Layer):
    """An activation layer just applies a function element wise to its inputs.
    """

    def __init__(self, f: F, f_prime: F) -> None:
        super(Activation, self).__init__()

        self._f: F = f
        self._f_prime: F = f_prime

    def forward(self, inputs: Tensor) -> Tensor:
        self.inputs = inputs
        return self._f(inputs)

    def backward(self, grad: Tensor) -> Tensor:
        """y = f(x) and x = g(z)
        => dy/dz = f'(x) * g'(z)
        """
        return self._f_prime(self.inputs) * grad


def tanh(x: Tensor) -> Tensor:
    return np.tanh(x)


def tanh_prime(x: Tensor) -> Tensor:
    y = np.tanh(x)
    return 1 - y**2


class Tanh(Activation):

    def __init__(self):
        super(Tanh, self).__init__(tanh, tanh_prime)


class SoftMax(Layer):
    """Normalise the vector to 1."""

    def __init__(self) -> None:
        super(SoftMax, self).__init__()

    def forward(self, inputs: Tensor) -> Tensor:
        self.inputs = inputs
        return np.exp(inputs) / np.sum(np.exp(inputs))

    def backward(self, grad: Tensor) -> Tensor:
        s = np.sum(np.exp(self.inputs))
        return np.exp(self.inputs) / s * (grad - np.sum(grad * np.exp(self.inputs)) / s)


class Convolution(Layer):
    """A Convolution layer of Neurons.
    https://leonardoaraujosantos.gitbooks.io/artificial-inteligence/content/convolution_layer.html
    """

    def __init__(self, filter: int, size: Tuple[int, int], depth: int, stride: int, pad: int) -> None:
        """Build a convolution layer.

        :param filter: Number of filter to apply.
        :param size: Kernel size.
        :type size: tuple
        :param depth: Depth of the input.
        :param stride: How much pixel to slide the window.
        :param pad: Number of pixel to use to zero-pad the input.
        """
        super(Convolution, self).__init__()

        self._depth = depth
        self._stride = stride
        self._pad = pad
        self._filter = filter

        self.params["w"] = np.random.randn(filter, size[0], size[1])
        self.params["b"] = np.random.randn(filter)

    def forward(self, inputs: Tensor) -> Tensor:
        N, C, H, W = inputs.shape
        F, C, HH, WW = self.params["w"].shape

        # Output size:
        H_R = 1 + (H + 2 * self._pad - HH) / self._stride
        W_R = 1 + (W + 2 * self._pad - WW) / self._stride

        out = np.zeros((N, F, H_R, W_R))
        x_pad = np.lib.pad(
            inputs,
            ((0, 0), (0, 0), (self._pad, self._pad), (self._pad, self._pad)),
            'constant',
            constant_values=0
        )

        for i in range(N):
            for depth in range(self._filter):
                for r in range(0, H, self._stride):
                    for c in range(0, W, self._stride):
                        out[i, depth, r/self._stride, c/self._stride] = np.sum(
                            x_pad[n, :, r:r+HH, c:c+WW] * self._params["w"][depth, :, :, :]
                        ) + self.params["b"][depth]

        return out

    def backward(self, grad: Tensor) -> Tensor:
        pass
