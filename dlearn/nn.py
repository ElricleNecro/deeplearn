# -*- coding: utf-8 -*-

"""A neural network is a collection of layers.
Its behavior is a lot similar to a layer itself.
"""

from typing import Sequence, Iterator, Tuple
from .tensor import Tensor
from .layers import Layer


class NeuralNet(object):

    def __init__(self, layers: Sequence[Layer]) -> None:
        self._layers: Sequence[Layer] = layers

    def forward(self, inputs: Tensor) -> Tensor:
        for layer in self._layers:
            inputs = layer.forward(inputs)

        return inputs

    def backward(self, grad: Tensor) -> Tensor:
        for layer in reversed(self._layers):
            grad = layer.backward(grad)

        return grad

    def params_and_grads(self) -> Iterator[Tuple[Tensor, Tensor]]:
        for layer in self._layers:
            for name, param in layer.params.items():
                grad = layer.grads[name]
                yield param, grad
