#! /usr/bin/env python3
# -*- coding:Utf8 -*-

# ------------------------------------------------------------------------------
# All necessary import:
# ------------------------------------------------------------------------------
import setuptools as st

from distutils.core import setup


def test_dependancy(*modules_list):
    import imp
    for deps in modules_list:
        try:
            imp.find_module(deps)
        except ImportError:
            print("Module %s not found." % deps)


packages = st.find_packages()

# ------------------------------------------------------------------------------
# Call the setup function:
# ------------------------------------------------------------------------------
setup(
    name='DeepLearn',
    version='0.1a',
    description='A simple Neural network based on http://www.youtube.com/watch?v=o64FV-ez6Gw',
    author='Guillaume Plum',
    packages=packages,
    install_requires=['numpy', 'pygments']
)

# vim:spelllang=
