# Deep Learn minimal library

This library is a little tool I build as an experiment. It is based on [this video](http://www.youtube.com/watch?v=o64FV-ez6Gw), then I build upon using multiple ressources.

## Requirements

Only numpy and at least python 3.6!

## How to use

Create inputs and targets `numpy.array` for your training data. Same for your test data. Then, create your neural net using all wanted layers. For example:
```python
from dlearn.nn import NeuralNet
from dlearn.layers import Tanh, Linear

net = NeuralNet([
    Linear(input_size=2, output_size=2),
    Tanh(),
    Linear(input_size=2, output_size=2),
])
```

To train the neural network on your dataset, use:
```python
from dlearn.train import train

train(net, inputs, targets, num_epochs=5000)
```

Then you can use your neural network using the forward method:
```python
predicted = net.forward(inputs)
```
